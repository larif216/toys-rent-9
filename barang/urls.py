from django.urls import path
from .views import *
app_name = 'barang'
urlpatterns = [
    path('daftar_barang', display_barang, name='daftar_barang'),
	path('detail_barang/<str:id>', detail_barang, name='detail_barang'),
	path('delete_barang/<str:id>', delete_barang, name='delete_barang'),
    path('update_barang/<str:id>', update_barang, name='update_barang'),
    path('create_barang', create_barang, name='create_barang'),
]
