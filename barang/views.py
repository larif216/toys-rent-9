from django.db import connection
from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from toysrent_db.models import *
from .forms import FormBarang, FormInfoBarangLevel

page_size = 10
# Create your views here
def update_barang(request, id):
    if request.session['role'] != 'admin':
        return HttpResponseRedirect(reverse('barang:daftar_barang'))
    barang = Barang.objects.raw('SELECT * FROM BARANG WHERE id_barang = %s', [id])
    info = InfoBarangLevel.objects.raw('SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s', [id])
    levels = LevelKeanggotaan.objects.raw('SELECT * FROM LEVEL_KEANGGOTAAN')
    formset = formset_factory(FormInfoBarangLevel, extra=len(levels), max_num=len(levels))
    initial_barang = {
        'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item.nama,
        'warna': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'no_ktp_penyewa': barang[0].no_ktp_penyewa.no_ktp.no_ktp
    }
    level = []
    initial_info = []
    for l in levels:
        exists = False
        for i in info:
            if l == i.nama_level:
                level.append(i.nama_level)
                initial_info.append({
                    'harga_sewa': i.harga_sewa,
                    'porsi_royalti': i.porsi_royalti
                })
                exists = True
                break
        if not exists:
            level.append(l)
            initial_info.append({
                'harga_sewa': '',
                'porsi_royalti': ''
            })

    if request.method == 'POST':
        form_barang = FormBarang(request.POST, prefix="barang", initial=initial_barang)
        form_info = formset(request.POST, prefix="info", initial=initial_info)
        if form_barang.is_valid() and form_info.is_valid():
            id_barang = initial_barang['id_barang']
            if form_barang.has_changed():
                for field in form_barang.changed_data:
                    if field != 'id_barang':
                        id_barang = form_barang.cleaned_data.get('id_barang')
                    data = [form_barang.cleaned_data.get(field), form_barang.cleaned_data.get('id_barang')]
                    with connection.cursor() as cursor:
                        cursor.execute(
                            'UPDATE barang SET %s = %s WHERE id_barang = %s' % (field, '%s', '%s'), data
                        )
            index = 0
            for form in form_info:
                harga_sewa = form.cleaned_data.get('harga_sewa')
                porsi_royalti = form.cleaned_data.get('porsi_royalti')
                with connection.cursor() as cursor:
                    cursor.execute(
                        'INSERT INTO INFO_BARANG_LEVEL VALUES (%s, %s, %s, %s) ON CONFLICT (id_barang, nama_level) DO UPDATE SET harga_sewa=%s, porsi_royalti=%s',
                        [id_barang, level[index].nama_level, harga_sewa, porsi_royalti, harga_sewa, porsi_royalti]
                    )
                    index += 1
            return HttpResponseRedirect(reverse('barang:daftar_barang'))
    form_barang = FormBarang(initial=initial_barang, prefix="barang")
    form_info = formset(prefix="info", initial=initial_info)
    level.reverse()
    return render(request, 'create_barang.html', {
        'barang': form_barang,
        'info': form_info,
        'level': level,
        'judul': 'Update'
    })


def save(barang, info_level, level):
    id_barang = barang.cleaned_data.get('id_barang')
    nama_item = barang.cleaned_data.get('nama_item').nama
    warna = barang.cleaned_data.get('warna')
    url_foto = barang.cleaned_data.get('url_foto')
    kondisi = barang.cleaned_data.get('kondisi')
    lama_penggunaan = barang.cleaned_data.get('lama_penggunaan')
    no_ktp_penyewa = barang.cleaned_data.get('no_ktp_penyewa').no_ktp.no_ktp
    data_barang = [id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa]

    with connection.cursor() as cursor:
        cursor.execute('INSERT INTO BARANG VALUES (%s, %s, %s, %s, %s, %s, %s)', data_barang)
        index = 0
        for info in info_level.forms:
            nama_level = level[index]
            harga_sewa = info.cleaned_data.get('harga_sewa')
            porsi_royalti = info.cleaned_data.get('porsi_royalti')
            cursor.execute('INSERT INTO INFO_BARANG_LEVEL VALUES (%s, %s, %s, %s)',
                           [id_barang, nama_level, harga_sewa, porsi_royalti])
            index += 1


def create_barang(request):
    if request.session['role'] != 'admin':
        return HttpResponseRedirect(reverse('barang:daftar_barang'))
    level_raw = LevelKeanggotaan.objects.raw('SELECT * FROM LEVEL_KEANGGOTAAN')
    level = [level.nama_level for level in level_raw]
    form_info = formset_factory(FormInfoBarangLevel, extra=len(level))
    if request.method == 'POST':
        form_barang = FormBarang(request.POST)
        form_info = form_info(request.POST, prefix='level')
        if form_info.is_valid() and form_barang.is_valid():
            save(form_barang, form_info, level)
            return HttpResponseRedirect(reverse('barang:daftar_barang'))
        else:
            return HttpResponseRedirect(reverse('barang:create_barang'))
    form_info = form_info(prefix='level')
    form_barang = FormBarang()
    level.reverse()
    return render(request, 'create_barang.html', {
        'barang': form_barang,
        'info': form_info,
        'level': level,
        'judul': 'Penambahan'
    })


def perform_delete(id):
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM barang where id_barang = %s', [id]
        )


def delete_barang(request, id):
    if request.session['role'] != 'admin':
        return HttpResponseRedirect(reverse('barang:daftar_barang'))
    perform_delete(id)
    return HttpResponseRedirect(reverse('barang:daftar_barang'))


def display_barang(request):
    sort_method = 'id_barang' if request.GET.get('sort', 'id_barang') == '' else request.GET.get('sort', 'id_barang')
    filter_method = None if request.GET.get('filter', None) == '' else request.GET.get('filter', None)
    kategori = list(Kategori.objects.raw('SELECT * FROM KATEGORI'))
    page_number = int(1 if request.GET.get('page', 1) == '' else request.GET.get('page', 1))

    if filter_method is not None:
        query = 'SELECT DISTINCT * FROM (SELECT id_barang, nama_item, warna, kondisi, nama_kategori FROM BARANG NATURAL JOIN KATEGORI_ITEM WHERE nama_kategori=\'{}\') AS N ORDER BY {}'.format(
            filter_method, sort_method)
    else:
        query = 'SELECT DISTINCT * FROM (SELECT id_barang, nama_item, warna, kondisi FROM BARANG NATURAL JOIN KATEGORI_ITEM) AS N ORDER BY {}'.format(
            sort_method)

    list_kategori = [
        {
            'nama': i.nama
        } for i in kategori
    ]
    barang = list(KategoriItem.objects.raw(query))
    data_barang = [
        {
            'id': i.id_barang,
            'nama': i.nama_item.nama,
            'warna': i.warna,
            'kondisi': i.kondisi
        } for i in barang
    ]
    return render(request, 'display_barang.html', {'barang': page_split(data_barang, page_number - 1), 'kategori': list_kategori, 'page': page_number,
        'pages': range(1, (len(data_barang) // page_size) + 1, )
})

def detail_barang(request, id):
    barang = Barang.objects.raw('SELECT * FROM BARANG WHERE id_barang = %s', [id])
    info = InfoBarangLevel.objects.raw('SELECT * FROM INFO_BARANG_LEVEL WHERE id_barang = %s', [id])
    levels = LevelKeanggotaan.objects.raw('SELECT * FROM LEVEL_KEANGGOTAAN')
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT BARANG_DIKIRIM.review, BARANG_DIKIRIM.tanggal_review FROM BARANG_DIKIRIM WHERE id_barang = %s',
            [id])
        review = cursor.fetchall()
    formset = formset_factory(FormInfoBarangLevel, extra=len(info), max_num=len(info))
    initial_barang = {
        'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item.nama,
        'warna': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'no_ktp_penyewa': barang[0].no_ktp_penyewa.no_ktp.no_ktp
    }
    level = []
    initial_info = []
    for l in levels:
        exists = False
        for i in info:
            if l == i.nama_level:
                level.append(i.nama_level)
                initial_info.append({
                    'harga_sewa': i.harga_sewa,
                    'porsi_royalti': i.porsi_royalti
                })
                exists = True
                break
        if not exists:
            level.append(l)
            initial_info.append({
                'harga_sewa': '',
                'porsi_royalti': ''
            })
    level.reverse()
    form_barang = FormBarang(initial=initial_barang)
    for i in form_barang:
        i.field.disabled = True
    form_info = formset(prefix='info', initial=initial_info)
    for form in form_info:
        for f in form:
            f.field.disabled = True
    return render(request, 'detail_barang.html', {
        'barang': form_barang,
        'info': form_info,
        'level': level,
        'review': review
    })

def page_split(collection, page_number):
    if collection is None or len(collection) == 0:
        return None
    return list(chunk(collection))[page_number]


def chunk(collection):
    for i in range(0, len(collection), page_size):
        yield collection[i:i + page_size]
