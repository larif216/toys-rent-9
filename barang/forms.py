from django import forms

from toysrent_db.models import Item, Anggota

class FormInfoBarangLevel(forms.Form):
    porsi_royalti = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'id': 'porsi_royalti',
                'class': 'form-control',
                'placeholder': 'Royalti'
            }
        )
    )

    harga_sewa = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'id': 'harga_sewa',
                'class': 'form-control',
                'placeholder': 'Harga sewa'
            }
        ),
    )

class FormBarang(forms.Form):
    id_barang = forms.CharField(
        max_length=10,
        widget=forms.TextInput(
            attrs={
                'id': 'id_barang',
                'class': 'form-control',
                'placeholder': 'ID Barang'
            }
        ),
        label='ID Barang'
    )

    nama_item = forms.ModelChoiceField(
        queryset=Item.objects.all(),
        widget=forms.Select(
            attrs={
                'id': 'nama_item',
                'class': 'form-control',
                'placeholder': 'Nama Item'
            }
        ),
        label='Nama Item',
        empty_label='Pilih nama item'
    )

    warna = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'id': 'warna',
                'class': 'form-control',
                'placeholder': 'Warna'
            },
        ),
        label='Warna',
        required=False
    )

    url_foto = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'id': 'url_foto',
                'class': 'form-control',
                'placeholder': 'URL Foto',
                'rows': '1'
            }
        ),
        label='URL',
        required=False
    )

    kondisi = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'id': 'kondisi',
                'class': 'form-control',
                'placeholder': 'Kondisi',
                'rows': '1'
            }
        ),
        label='Kondisi'
    )

    lama_penggunaan = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'id': 'lama_penggunaan',
                'class': 'form-control',
                'placeholder': 'Lama penggunaan dalam bulan'
            }
        ),
        label='Lama Penggunaan',
        required=False
    )

    no_ktp_penyewa = forms.ModelChoiceField(
        queryset=Anggota.objects.all(),
        widget=forms.Select(
            attrs={
                'id': 'no_ktp_penyewa',
                'class': 'form-control',
                'placeholder': 'Nama Pemilik'
            }
        ),
        label='Nama Pemilik',
        empty_label='Pilih nama pemilik barang'
    )
