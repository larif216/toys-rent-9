from django.urls import path
from .views import *

app_name = 'level'
urlpatterns = [
    path('', createlevel, name='createlevel'),
    path('daftar_level', daftarlevel, name='daftarlevel'),
    path('update_level', updatelevel, name='updatelevel'),
]
