from django import forms

from .models import *
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator



class FormLevel(forms.Form):
    nama_level = forms.CharField(
        label='Nama Level',
        label_suffix = "",
        widget=forms.TextInput(
            attrs={
                'id': 'nama_level',
                'class': 'form-control'
            }
        )
    )

    minimum_poin = forms.IntegerField(
        label='Minimum Poin',
        widget=forms.NumberInput(
            attrs={
                'id': 'minimum_poin',
                'class': 'form-control'
            }
        )
    )

    deskripsi = forms.CharField(
        label='Deskripsi',
        widget=forms.Textarea(
            attrs={
                'id': 'deskripsi',
                'class': 'form-control'
            }
        )
    )



    