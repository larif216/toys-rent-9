from django.shortcuts import render
from django.db import connection
from django.forms import formset_factory #alamat
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from toysrent_db.models import *
from .forms import FormLevel

# Create your views here.


def createlevel(request):
    levelForm = FormLevel(request.POST)
    if (request.method == 'POST' and levelForm.is_valid()):
        with connection.cursor() as cursor:
            nama_level = request.POST['nama_level']
            minimum_poin = request.POST['minimum_poin']
            deskripsi = request.POST['deskripsi']
            cursor.execute("INSERT INTO LEVEL_KEANGGOTAAN VALUES(%s, %s, %s)", [nama_level, minimum_poin, deskripsi])
    return render(request, 'create_level.html')


def updatelevel(request):
    response = {}
    data = {}
    levelForm = FormLevel(request.POST)
    if (request.method == 'POST' and levelForm.is_valid()):
        with connection.cursor() as cursor:
            nama_level = request.POST['nama_level']
            minimum_poin = request.POST['minimum_poin']
            deskripsi = request.POST['deskripsi']
            cursor.execute("UPDATE LEVEL_KEANGGOTAAN SET nama_level = %s, minimum_poin = %s, deskripsi = %s WHERE nama_level = %s", [nama_level, minimum_poin, deskripsi, nama_level])
        return render(request, 'daftar_level.html', response)
    
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM LEVEL_KEANGGOTAAN K WHERE K.nama_level = %s", [nama_level])
        data_level = cursor.fetchone()

    data["nama_level"] = data_level[0]
    data["minimum_poin"] = data_level[1]
    data["deskripsi"] = data_level[2]

    form_level = levelForm(data)
    response["form_level"] = form_level
    response["nama_level"] = nama_level
    print(form_level)
    return render(request, 'update_level.html', response)


def daftarlevel(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_level, minimum_poin, deskripsi FROM LEVEL_KEANGGOTAAN")
        tupel = cursor.fetchall()
    response['level'] = tupel
    return render(request, 'daftar_level.html', response)

def deletelevel(request, nama):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM LEVEL_KEANGGOTAAN WHERE nama_level= %s", [nama_level])
    return render(request, 'create_level.html', response)
