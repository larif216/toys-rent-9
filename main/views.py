from django.shortcuts import render
from .forms import *
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection
from toysrent_db.models import *

# Create your views here.
response = {'author':'admin-toys-rent'}
response['login'] = Login_Form
def index(request):
	return render(request, 'main.html', response)

def reg_admin(request):
	response['regis'] = Register_Form_Admin
	return render(request, 'register_for_admin.html', response)

def reg_anggota(request):
	response['regis'] = Register_Form_Anggota
	return render(request, 'register_for_anggota.html', response)

def save_admin(request):
	if request.method == 'POST':
		no_ktp = request.POST['no_ktp']
		nama = request.POST['nama_lengkap']
		email = request.POST['email']
		tanggal_lahir = request.POST['tanggal_lahir']
		no_telp = request.POST['nomor_telepon']
		request.session['role'] = 'admin'
		request.session['no_ktp'] = no_ktp
		with connection.cursor() as c:
			ktp_admin = "INSERT INTO ADMIN VALUES('%s')" % (no_ktp)
			sql = "INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) VALUES ('%s', '%s', '%s', '%s', '%s')" % (no_ktp, nama, email, tanggal_lahir, no_telp)
			c.execute(sql)
			c.execute(ktp_admin)
			return HttpResponseRedirect(reverse('login:auth')) #return pada admin
		#return default

def save_anggota(request):
	if request.method == 'POST':
		no_ktp = request.POST['no_ktp']
		nama = request.POST['nama_lengkap']
		email = request.POST['email']
		tanggal_lahir = request.POST['tanggal_lahir']
		no_telp = request.POST['nomor_telepon']
		nama_alamat = request.POST['nama_alamat']
		jalan = request.POST['jalan']
		nomor = request.POST['nomor']
		kota = request.POST['kota']
		kodepos = request.POST['kode_pos']
		request.session['role'] = 'anggota'
		request.session['no_ktp'] = no_ktp
		with connection.cursor() as c:
			alamat = "INSERT INTO ALAMAT VALUES ('%s', '%s', '%s', '%s', '%s', '%s')" % (no_ktp, nama_alamat, jalan, nomor, kota, kodepos)
			data = "INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) VALUES ('%s', '%s', '%s', '%s', '%s')" % (no_ktp, nama, email, tanggal_lahir, no_telp)
			anggota = "INSERT INTO ANGGOTA VALUES ('%s', 0, 'Bronze')" % (no_ktp)
			c.execute(data)
			c.execute(anggota)
			c.execute(alamat)
			return HttpResponseRedirect(reverse('login:auth'))


	