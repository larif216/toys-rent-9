from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from .models import *

attrs_name = {
    'class': 'form-control',
    'size': 7,
    'placeholder': 'Masukkan nama lengkap Anda'
}

attrs_date = {
    'class': 'form-control',
    'size': 7,
    'type': 'date'
}

attrs_email = {
    'class': 'form-control',
    'size': 7,
    'placeholder': 'Masukkan email valid Anda'
}

attrs_no_ktp = {
    'class': 'form-control',
    'size': 7,
    'placeholder': 'Masukkan nomer KTP Anda'
}

attrs_confirm = {
    'class': 'form-control',
    'size': 7,
    'placeholder': 'Masukkan nomer telepon Anda'
}

class Register_Form_Admin(forms.Form):
    no_ktp = forms.CharField(label='NO KTP', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    nama_lengkap = forms.CharField(label='NAMA LENGKAP', required=True, widget=forms.widgets.TextInput(attrs=attrs_name))
    email = forms.EmailField(label='EMAIL', required=True, widget=forms.widgets.EmailInput(attrs=attrs_email))
    tanggal_lahir = forms.DateField(label='TANGGAL LAHIR', required=True, widget=forms.widgets.DateInput(attrs=attrs_date))
    nomor_telepon = forms.CharField(label='NOMOR TELEPON', required=True, widget=forms.widgets.TextInput(attrs=attrs_confirm))


class Register_Form_Anggota(forms.Form):
    no_ktp = forms.CharField(label='No KTP', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    nama_lengkap = forms.CharField(label='Nama Lengkap', required=True, widget=forms.widgets.TextInput(attrs=attrs_name))
    email = forms.EmailField(label='Email', required=True, widget=forms.widgets.EmailInput(attrs=attrs_email))
    tanggal_lahir = forms.DateField(label='Tangal Lahir', required=True, widget=forms.widgets.DateInput(attrs=attrs_date))
    nomor_telepon = forms.CharField(label='Nomor Telepon', required=True, widget=forms.widgets.TextInput(attrs=attrs_confirm))
    nama_alamat = forms.CharField(label='Alamat', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    jalan = forms.CharField(label='Jalan', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    nomor = forms.CharField(label='Nomer', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    kota = forms.CharField(label='Nama Kota', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))
    kode_pos = forms.CharField(label='Kode Post', required=True, widget=forms.widgets.TextInput(attrs=attrs_no_ktp))


class Login_Form(forms.Form):
    attrs_no_ktp = {
    'class' : 'form-control',
    'placeholder' : 'Nomer KTP',
    }

    attrs_email = {
    'class' : 'form-control',
    'placeholder' : 'email',
    }

    no_ktp = forms.CharField(label = 'NO KTP', required = True, widget = forms.widgets.TextInput(attrs = attrs_no_ktp))
    email = forms.EmailField(label = 'Email', required = True, widget = forms.widgets.EmailInput(attrs = attrs_email))
