from django.urls import path
from .views import *

app_name = 'main'
urlpatterns = [
	path('', index, name='index'),
	path('admin', reg_admin, name="admin"),
	path('anggota', reg_anggota, name="anggota"),
	path('reg_admin', save_admin, name="register_admin"),
	path('reg_anggota', save_anggota, name="register_anggota"),   
]
