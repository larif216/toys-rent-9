from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Login(models.Model):
	"""docstring for ClassName"""
	no_ktp = models.CharField(max_length=100)
	email = models.EmailField(max_length=50)

class Register_Admin(models.Model):
    no_ktp = models.CharField(max_length=100, unique = True)
    nama_lengkap = models.CharField(max_length = 100)
    email = models.EmailField(max_length = 50, unique = True, default = False)
    tanggal_lahir = models.DateField()
    nomor_telepon = models.CharField(max_length = 100)

class Register_Anggota(models.Model):
	"""docstring for ClassName"""
	pengguna = models.ForeignKey('Register_Admin', on_delete = models.CASCADE)
	alamat = models.ForeignKey('Alamat', on_delete = models.CASCADE)

		
class Alamat(models.Model):
	"""docstring for ClassName"""
	nama_alamat = models.CharField(max_length=50)
	jalan = models.CharField(max_length=50)
	nomor = models.CharField(max_length=50)
	kota = models.CharField(max_length=50)
	kode_post = models.CharField(max_length=50)