from django.apps import AppConfig


class ToysrentDbConfig(AppConfig):
    name = 'toysrent_db'
