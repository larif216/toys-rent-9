from django.urls import path
from .views import *

app_name = 'login'
urlpatterns = [
	path('', auth_login , name='auth'),
	path('logout', logout, name='logout'),   
]
