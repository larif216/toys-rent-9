from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from toysrent_db.models import *


def auth_login(request):
	if request.method == "POST":
		user_ktp = request.POST["no_ktp"]
		user_email = request.POST["email"]
		data = [user_ktp, user_email]

		if user_is_exist(user_ktp, user_email):
			request.session['no_ktp'] = user_ktp
			request.session['email'] = user_email
			if Anggota.objects.raw("SELECT * FROM ANGGOTA WHERE no_ktp = %s", [user_ktp, ]):
				request.session['role'] = 'anggota'
				return HttpResponseRedirect(reverse('barang:daftar_barang')) #daftar barang
			else:
				request.session['role'] = 'admin'
				return HttpResponseRedirect(reverse('pesanan:daftar_pesanan_admin')) #daftar pemesanan
		else:
			msg = "no_ktp./email Anda Salah atau Akun Tidak Ada"
			messages.error(request, msg)
			return HttpResponseRedirect(reverse('main:index'))

	if 'role' in request.session:
		if request.session['role'] == 'admin':
			return HttpResponseRedirect(reverse('pesanan:daftar_pesanan_admin')) #daftar pemesanan
		else:
			return HttpResponseRedirect(reverse('barang:daftar_barang')) #daftar barang
		
	return render(request, 'main.html')

def user_is_exist(ktp,email):
	available_user = Pengguna.objects.raw("SELECT * FROM PENGGUNA WHERE no_ktp = %s AND email = %s", [ ktp, email])
	return len(list(available_user)) != 0

def logout(request):
	request.session.flush()
	return HttpResponseRedirect(reverse('main:index'))