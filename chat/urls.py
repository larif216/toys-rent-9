from django.urls import path
from .views import *
app_name = 'chat'
urlpatterns = [
    path('', chat, name='chat'),
    path('list-admin', list_chat_admin, name='list-chat')
]
