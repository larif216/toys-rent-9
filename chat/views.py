from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime

# Create your views here.
from django.urls import reverse

from toysrent_db.models import Chat


def chat(request):
    no_ktp = request.session['no_ktp']
    if request.method == 'POST':
        latest_row = Chat.objects.raw('SELECT * from chat order by id::int DESC LIMIT 1')
        latest_id = latest_row[0].id
        latest_id = latest_id.replace('-', '')
        latest_id = complex(latest_id)
        pesan = request.POST['pesan']
        if request.session['role'] == 'admin':
            user = request.GET.get('user-ktp')
            with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO CHAT values (%s, %s, %s, %s, %s, %s)", [str(latest_id+1), pesan, datetime.now(), user, no_ktp, True])
        else:
            with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO CHAT values (%s, %s, %s, %s, %s, %s)", [str(latest_id+1), pesan, datetime.now(), no_ktp, '16911220-3196', False])
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    if request.session['role'] == 'admin':
        user = request.GET.get('user-ktp')
        chats = Chat.objects.raw('Select * from chat where no_ktp_anggota = %s', [user])
        return render(request, 'chat_anggota.html', {'chats': chats})
    elif request.session['role'] == 'anggota':
        chats = Chat.objects.raw('Select * from chat where no_ktp_anggota = %s', [no_ktp])
        return render(request, 'chat_anggota.html',  {'chats': chats})

def list_chat_admin(request):
    if request.session['role'] == 'anggota':
        return HttpResponseRedirect(reverse('item:display_item'))
    with connection.cursor() as cursor:
        cursor.execute('SELECT distinct(no_ktp_anggota) from CHAT,pengguna where chat.no_ktp_anggota = pengguna.no_ktp')
        list_no = cursor.fetchall()
        list_chat = []
        for nomor in list_no:
            cursor.execute('Select nama_lengkap from pengguna where no_ktp = %s', [nomor])
            res = cursor.fetchall()
            res.append(nomor)
            list_chat.append(res)
    return render(request, 'chat_admin.html', {'chats': list_chat})
