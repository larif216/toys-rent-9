from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection

from toysrent_db.models import *
# Create your views here.
response = {'author':'toys-rent-e9'}
def display(request):
	item = list(Item.objects.raw('SELECT * FROM ITEM'))
	count = 1
	data = []
	for i in item:
		x = {
			'nomor' : count,
			'nama' : i.nama,
			'kategori' : i.deskripsi} 
		data.append(x)
		count = count+1
	response['item'] = data
	return render(request, 'display_item.html', response)


def add_item(request):
	if request.method == 'POST':
		nama = request.POST['nama_item']
		deskripsi = request.POST['deskripsi']
		rentang_usia = request.POST['rentang_usia'].split('-')
		usia_dari = int(rentang_usia[0])
		usia_sampai = int(rentang_usia[1])
		bahan = request.POST['bahan']
		with connection.cursor() as c:
			sql = "INSERT INTO item (nama, deskripsi, usia_dari, usia_sampai, bahan) VALUES ('%s','%s',%d,%d,'%s')" % (nama, deskripsi, usia_dari, usia_sampai, bahan)
			c.execute(sql)
			return HttpResponseRedirect(reverse('item:display_item'))
		return
	else:
		return render(request, 'tambah_item.html', response)

def delete_item(request, nama):
	with connection.cursor() as c:
		c.execute("DELETE FROM item WHERE nama= %s", [nama])
		return HttpResponseRedirect(reverse('item:display_item'))

	return render(request, 'tambah_item.html', response)

def update_item(request):
	return render(request, 'update_item.html', response)


def pilih_item(request, nama):
	select_item = list(Item.objects.raw('SELECT * FROM ITEM WHERE nama = %s', [nama]))
	item = []
	for x in select_item:
		data = {
			'nama': x.nama,
			'deskripsi' : x.deskripsi,
			'rentang_usia' : str(x.usia_dari) + '-' + str(x.usia_sampai),
			'bahan': x.bahan 
		}
		item.append(data)
	response['item'] = item
	return render(request, 'update_item.html', response)