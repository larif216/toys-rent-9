from django.urls import path
from .views import *

app_name = 'item'
urlpatterns = [
	path('', display, name='display_item'),
	path('add/', add_item, name='add_item'),
	path('delete_item/<str:nama>/', delete_item, name='delete_item'),
	path('pilih_item/<str:nama>/', pilih_item, name='pilih_item'),
	path('update_item/<str:nama>/', update_item, name='update_item'),
]
