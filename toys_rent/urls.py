"""toys_rent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView

import main.urls
import level.urls
import pesanan.urls
import item.urls
import pengiriman.urls
import barang.urls
import chat.urls
import login.urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='home/', permanent=True)),
    path('home/', include(main.urls)),
    path('barang/', include(barang.urls)),
    path('level/', include(level.urls)),
    path('pesanan/', include(pesanan.urls)),
    path('item/', include(item.urls)),
    path('pengiriman/', include(pengiriman.urls)),
    path('chat/', include(chat.urls)),
    path('login/', include(login.urls))
]
