from django.urls import path
from .views import *
app_name='pengiriman'
urlpatterns = [
	path('', daftar_pengiriman, name='daftar_pengiriman'),
	#path('daftar_pengiriman_admin', daftar_pengiriman_admin, name = 'daftar_pengiriman_admin'),
	path('add_pengiriman', add_pengiriman, name='add_pengiriman'),
	path('delete_pengiriman/<str:id>', delete_pengiriman, name='delete_pengiriman'),
	path('delete_review/<str:id>', delete_review, name='delete_review'),
	path('daftar_review', daftar_review, name='daftar_review'),
	path('add_review', add_review, name='add_review'),
	path('update_pengiriman/<str:id>', update_pengiriman, name='update_pengiriman')]
	#path('daftar_pengiriman_anggota', create_pengiriman_anggota, name = 'create_pengiriman_anggota'),
	#path('update_pengiriman_admin', update_pengiriman_admin, name = 'update_pengiriman_admin'),
	#path('update_pengiriman_anggota', update_pengiriman_anggota, name = 'update_pengiriman_anggota')]