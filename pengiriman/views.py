from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
from time import gmtime, strftime
from .forms import *
from toysrent_db.models import *
import random
import string
response = {'author':'toys-rent-e9'}
# Create your views here.
def daftar_pengiriman(request):
	item = list(Pengiriman.objects.raw('SELECT * FROM PENGIRIMAN'))
	barang = list(Pengiriman.objects.raw('SELECT *, P.no_resi  AS nresi, id_pemesanan, nama_item FROM PENGIRIMAN P, BARANG_DIKIRIM BD, BARANG B WHERE P.no_resi = BD.no_resi AND B.id_barang = BD.id_barang'))
	hrgtotal = list(Pengiriman.objects.raw('SELECT *, P.no_resi AS nresi, harga_sewa FROM PENGIRIMAN P, BARANG_DIKIRIM BD, ANGGOTA A, INFO_BARANG_LEVEL I WHERE P.no_resi = BD.no_resi AND BD.id_barang = I.id_barang AND A.no_ktp = P.no_ktp_anggota AND A.level = I.nama_level'))
	count = 1
	data = []
	for i in item:
		total = 0
		a = ""
		for e in barang:
			if(e.nresi == i.no_resi) :
				st = len(str(e.id_pemesanan))
				a = a + str(e.nama_item) + " - " + str(e.id_pemesanan)[18:st-1] + "\n"
		for u in hrgtotal :
			if(u.nresi == i.no_resi) :
				total += u.harga_sewa
		total += i.ongkos
		x = {
			'no_resi' : i.no_resi,
			'id_pengiriman' : count,
			'daftarbp' : a,
			'hargatotal' : total,
			'tgl' : i.tanggal} 
		data.append(x)
		count = count+1
	response['item'] = data
	return render(request, 'daftar_pengiriman.html', response)
#def daftar_pengiriman_anggota(request):
#	return render(request, 'daftar_pengiriman_anggota.html')
def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))
def save(pengiriman):
	no_resi = randomString(10)
	id_pemesanan = pengiriman.cleaned_data.get('barang_pesanan').id_pemesanan.id_pemesanan
	metode = pengiriman.cleaned_data.get('metode')
	ongkos = pengiriman.cleaned_data.get('barang_pesanan').id_pemesanan.ongkos
	tanggal = pengiriman.cleaned_data.get('tanggal_pengiriman')
	no_ktp_anggota = pengiriman.cleaned_data.get('alamat').no_ktp_anggota.no_ktp.no_ktp
	##alamats = Alamat.objects.raw('SELECT * FROM ALAMAT WHERE no_ktp = %s', no_ktp_anggota)
	nama_alamat_anggota = pengiriman.cleaned_data.get('alamat').nama
	data_pengiriman = [no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota]
	no_urut = randomString(10)
	id_barang = pengiriman.cleaned_data.get('barang_pesanan').id_barang.id_barang
	tanggal_review = '2019-05-29 00:00:00'
	review = 'default review'
	data_barang_dikirim = [no_resi, id_barang, tanggal_review, review, no_urut]
	with connection.cursor() as cursor:
		cursor.execute('INSERT INTO PENGIRIMAN VALUES (%s, %s, %s, %s, %s, %s, %s)', data_pengiriman)
		cursor.execute('INSERT INTO BARANG_DIKIRIM VALUES (%s, %s, %s, %s, %s)', data_barang_dikirim)

def add_pengiriman(request):
	if request.method == 'POST':
		print(request.POST)
		form_pengiriman = FormPengiriman(request.POST)
		if form_pengiriman.is_valid():
			save(form_pengiriman)
			return HttpResponseRedirect(reverse('pengiriman:daftar_pengiriman'))
		else:
			return HttpResponseRedirect(reverse('pengiriman:add_pengiriman'))
	form_pengiriman = FormPengiriman()
	return render(request, 'create_pengiriman.html', {'pengiriman': form_pengiriman})

def update_pengiriman(request, id):
	pengiriman = Pengiriman.objects.raw('SELECT * FROM PENGIRIMAN WHERE no_resi = %s', [id])
	brgpsn = BarangPesanan.objects.raw('SELECT * FROM BARANG_PESANAN BP, PENGIRIMAN P WHERE BP.id_pemesanan = P.id_pemesanan AND P.no_resi = %s', [id])
	barang_dikirim = BarangDikirim.objects.raw('SELECT * FROM BARANG_DIKIRIM WHERE no_resi = %s', [id])
	initial_pengiriman = {
		'barang_pesanan' : brgpsn[0],
		'alamat' : pengiriman[0].no_ktp_anggota,
		'metode' : pengiriman[0].metode,
		'tanggal_pengiriman' : pengiriman[0].tanggal
	}
	if request.method == 'POST':
		form_pengiriman = FormPengiriman(request.POST, prefix="pengiriman", initial=initial_pengiriman)
		if form_pengiriman.is_valid() :
			no_resi = initial_pengiriman['no_resi']
			if form_pengiriman.has_changed():
				for field in form_pengiriman.changed_data:
					if field == 'barang_pesanan' :
						id_pemesanan = form_pengiriman.cleaned_data.get(field).id_pemesanan.id_pemesanan
						ongkos = form_pengiriman.cleaned_data.get(field).id_pemesanan.ongkos
						data = [id_pemesanan, ongkos, no_resi]
						id_barang = form_pengiriman.cleaned_data.get(field).id_barang.id_barang
						datakrm = [id_barang, no_resi]
						with connection.cursor() as cursor:
							cursor.execute( 'UPDATE PENGIRIMAN SET id_pemesanan = %s, ongkos = %s WHERE no_resi = %s', data)
							cursor.execute( 'UPDATE BARANG_DIKIRIM SET id_barang = %s,WHERE no_resi = %s', datakrm)
					elif field == 'alamat' :
						no_ktp_anggota = form_pengiriman.cleaned_data.get(field).no_ktp_anggota.no_ktp.no_ktp
						nama_alamat_anggota = form_pengiriman.cleaned_data.get(field)
						data = [no_ktp_anggota, nama_alamat_anggota, no_resi]
						with connection.cursor() as cursor:
							cursor.execute( 'UPDATE PENGIRIMAN SET no_ktp_anggota = %s, nama_alamat_anggota = %s WHERE no_resi = %s', data)
					elif field == 'tanggal_pengiriman' :
						tanggal = form_pengiriman.cleaned_data.get(field)
						data = [tanggal, no_resi]
						with connection.cursor() as cursor:
							cursor.execute( 'UPDATE PENGIRIMAN SET tanggal = %s WHERE no_resi = %s', data)
					else :
						metode = form_pengiriman.cleaned_data.get(field)
						data = [metode, no_resi]
						with connection.cursor() as cursor:
							cursor.execute( 'UPDATE PENGIRIMAN SET metode = %s WHERE no_resi = %s', data)
			return HttpResponseRedirect(reverse('pengiriman:daftar_pengiriman'))
	form_pengiriman = FormPengiriman(initial=initial_pengiriman, prefix="pengiriman")
	return render(request, 'create_pengiriman.html', {
        'pengiriman': form_pengiriman,
        'judul': 'Update'
    })
def perform_delete(id):
	with connection.cursor() as cursor:
		cursor.execute(
			'DELETE FROM BARANG_DIKIRIM where no_resi = %s', [id]
		)
		cursor.execute(
			'DELETE FROM PENGIRIMAN where no_resi = %s', [id]
		)
def delete_pengiriman(request, id):
	print(id)
	perform_delete(id)
	return HttpResponseRedirect(reverse('pengiriman:daftar_pengiriman'))
def daftar_review(request) :
	item = list(BarangDikirim.objects.raw('SELECT * FROM BARANG_DIKIRIM'))
	nam = list(Barang.objects.raw('SELECT * FROM BARANG B, BARANG_DIKIRIM BD WHERE BD.id_barang = B.id_barang'))
	count = 1
	data = []
	for i in item:
		total = 0
		st = ""
		for e in nam:
			if(e.id_barang == i.id_barang) :
				st = e.nama
		x = {
			'no_urut' : i.no_urut,
			'id_pengiriman' : i.no_resi.no_resi,
			'nama' : i.id_barang.nama_item,
			'review' : i.review,
			'tgl' : i.tanggal_review} 
		data.append(x)
		count = count+1
	response['item'] = data
	return render(request, 'daftar_review.html', response)

def add_review(request):
	if request.method == 'POST':
		print(request.POST)
		form_review = FormReview(request.POST)
		if form_review.is_valid():
			apdet(form_review)
			return HttpResponseRedirect(reverse('pengiriman:daftar_review'))
		else:
			return HttpResponseRedirect(reverse('pengiriman:add_review'))
	form_review = FormReview()
	return render(request, 'create_review.html', {'review': form_review})
def apdet(review):
	no_urut = review.cleaned_data.get('barang').no_urut
	kalreview = review.cleaned_data.get('review')
	tgl = strftime("%Y-%m-%d %H:%M:%S", gmtime())
	data = [kalreview, tgl, no_urut]
	with connection.cursor() as cursor:
		cursor.execute( 'UPDATE BARANG_DIKIRIM SET review = %s, tanggal_review = %s WHERE no_urut = %s', data)
def perform_delete2(id):
	with connection.cursor() as cursor:
		cursor.execute(
			'DELETE FROM BARANG_DIKIRIM where no_urut = %s', [id]
		)
def delete_review(request, id):
	print(id)
	perform_delete2(id)
	return HttpResponseRedirect(reverse('pengiriman:daftar_review'))