from django.urls import path
from .views import *

app_name = 'pesanan'
urlpatterns = [
    path('', pesanan_anggota, name='pesanan_anggota'),
    path('pesanan_admin', pesanan_admin, name='pesanan_admin'),
    path('update_pesanan', update_pesanan, name='update_pesanan'),
    path('daftar_pesanan', daftar_pesanan_anggota,
         name='daftar_pesanan_anggota'),
    path('daftar_pesanan_admin', daftar_pesanan_admin,
         name='daftar_pesanan_admin'),
]
