from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

from toysrent_db.models import *


# Create your views here.
response = {'author':'toys-rent-e9'}

def pesanan_anggota(request):
    response = {}
    if request.method == 'POST':
		# nama = request.POST['barang']
        # with connection.cursor() as cursor:
		# 	sql = "INSERT INTO PEMESANAN (id_pemesanan, (harga_sewa + ongkos), status) VALUES (%s,%d,'%s')" % (id_pemesanan, (harga_sewa + ongkos), status)
		# 	cursor.execute(sql)
		# 	return HttpResponseRedirect(reverse('item:display_item'))
        return
    else:
        return render(request, 'pesanan_anggota.html', response)



def pesanan_admin(request):
    response = {}
    return render(request, 'pesanan_admin.html', response)


def update_pesanan(request):
    response = {}
    return render(request, 'update_pesanan.html', response)


def daftar_pesanan_anggota(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_pemesanan, (harga_sewa + ongkos), status FROM PEMESANAN")
        seluruh_pesanan = cursor.fetchall()
        count = 0
        for row in seluruh_pesanan:
            cursor.execute("SELECT b.nama_item from BARANG as B, BARANG_PESANAN as BP, PEMESANAN as P WHERE B.id_barang = BP.id_barang AND BP.id_pemesanan = P.id_pemesanan AND P.id_pemesanan = \'{}\'".format(row[0]))
            barang_dipesan = cursor.fetchall()
            barang_dipesan_tuple = tuple()
            for item in barang_dipesan:
                barang_dipesan_tuple += item 
            seluruh_pesanan[count] += (barang_dipesan_tuple,)
            count += 1
    response['daftar_pesanan_anggota'] = seluruh_pesanan
    return render(request, 'daftar_pesanan_anggota.html', response)


def daftar_pesanan_admin(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_pemesanan, (harga_sewa + ongkos), status FROM PEMESANAN")
        seluruh_pesanan = cursor.fetchall()
        count = 0
        for row in seluruh_pesanan:
            cursor.execute("SELECT b.nama_item from BARANG as B, BARANG_PESANAN as BP, PEMESANAN as P WHERE B.id_barang = BP.id_barang AND BP.id_pemesanan = P.id_pemesanan AND P.id_pemesanan = \'{}\'".format(row[0]))
            barang_dipesan = cursor.fetchall()
            barang_dipesan_tuple = tuple()
            for item in barang_dipesan:
                barang_dipesan_tuple += item 
            seluruh_pesanan[count] += (barang_dipesan_tuple,) 
            count += 1
    response['daftar_pesanan_admin'] = seluruh_pesanan
    return render(request, 'daftar_pesanan_admin.html', response)


def delete_pesanan(request, nama):
	# with connection.cursor() as cursor:
	# 	cursor.execute("DELETE FROM PEMESANAN WHERE nama= %s", [nama])
	# 	return HttpResponseRedirect(reverse('item:display_item'))
	return render(request, 'pesanan_anggota.html', response)




