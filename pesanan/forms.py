from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from .models import *


class FormBarangAdmin(forms.Form):
    anggota = forms.CharField(
        label='Anggota', 
        required=True, 
        widget=forms.widgets.TextInput(
            attrs={
                'id': 'anggota',
                'class': 'form-control'
            }
        )
    )
    
    pilih_barang = forms.IntegerField(
        label='Barang', 
        required=True, 
        widget=forms.widgets.TextInput(
            attrs={
                'id': 'pilih_barang',
                'class': 'form-control'
            }
            )
        )
    
    lama_sewa = forms.CharField(
        label='Lama Sewa', 
        required=True, 
        widget=forms.widgets.CharInput(
            attrs={
                'id': 'lama_sewa',
                'class': 'form-control'
            }
            )
        )
    

class FormBarangAnggota(forms.Form):
    pilih_barang = forms.IntegerField(
        label='Barang', 
        required=True, 
        widget=forms.widgets.TextInput(
            attrs={
                'id': 'pilih_barang',
                'class': 'form-control'
            }
            )
        )
    
    lama_sewa = forms.CharField(
        label='Lama Sewa', 
        required=True, 
        widget=forms.widgets.CharInput(
            attrs={
                'id': 'lama_sewa',
                'class': 'form-control'
            }
        )
    )
  