
$(document).ready(function() {

        var countDownDate = new Date("May 27, 2019 23:55:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in an element with id="demo"
            document.getElementsByClassName("days")[0].innerHTML = days;
            document.getElementsByClassName("hours")[0].innerHTML = hours;
            document.getElementsByClassName("minutes")[0].innerHTML = minutes;
            document.getElementsByClassName("seconds")[0].innerHTML = seconds;
        }
        )
    }
)
