from django.urls import path
from .views import *

app_name = 'coming_soon'
urlpatterns = [
    path('', index, name='index'),
]
